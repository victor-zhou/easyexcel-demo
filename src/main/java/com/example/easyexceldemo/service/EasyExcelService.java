package com.example.easyexceldemo.service;

import cn.hutool.core.io.FileUtil;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.util.FileUtils;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.alibaba.excel.write.metadata.fill.FillConfig;
import com.example.easyexceldemo.dto.DataDmo;
import com.example.easyexceldemo.dto.RangeDto;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class EasyExcelService {


    public String exportData(List<DataDmo> exportData, String title) {
        // 模板路径
        String excelTemplate = this.getClass().getResource("/template/statistic.xlsx").getFile();
        // 生成路径
        String fileName = FileUtil.touch("C:/excel/" + title + ".xlsx").getAbsolutePath();
        // 生成excel
        generateExcel(exportData, title, excelTemplate, fileName);
        return fileName;
    }

    public void generateExcel(List<DataDmo> exportData, String title, String templatePath, String targetPath) {
        Map<String, List<RangeDto>> rowMergeMap = addRowMergeStrategy(exportData);
        Map<String, List<RangeDto>> colMergeMap = addColMergeStrategy(exportData);
        ExcelWriter excelWriter = EasyExcel.write(targetPath).withTemplate(templatePath).build();
        WriteSheet writeSheet = EasyExcel.writerSheet().registerWriteHandler(new BizMergeStrategy(rowMergeMap, colMergeMap)).build();
        FillConfig fillConfig = FillConfig.builder().forceNewRow(Boolean.TRUE).build();
        excelWriter.fill(exportData, fillConfig, writeSheet);
        Map<String, Object> map = new HashMap<>();
        map.put("title", title);
        excelWriter.fill(map, writeSheet);
        excelWriter.finish();
    }

    /**
     * @description: 列表导出--添加行合并策略（EasyExcel）
     */
    public static Map<String, List<RangeDto>> addRowMergeStrategy(List<DataDmo> exportData) {
        //key为行，value为列，List中保存的是列的起始位置和结束位置索引
        Map<String, List<RangeDto>> strategyMap = new HashMap<>();
        // 当前行的上一行数据
        DataDmo preExcelDto = null;
        DataDmo currDto = null;
        for (int i = 0; i < exportData.size(); i++) {
            //当前行的数据
            currDto = exportData.get(i);
            if (preExcelDto != null) {
                //如果name一样，则可合并一列
                if (currDto.getName().equals(preExcelDto.getName())) {
                    int n = i + 1;
                    fillStrategyMap(strategyMap, "0", n);
                }
            }
            preExcelDto = currDto;
        }
        return strategyMap;
    }

    /**
     * @description: 列表导出--添加列合并策略（EasyExcel）
     */
    public static Map<String, List<RangeDto>> addColMergeStrategy(List<DataDmo> exportData) {
        Map<String, List<RangeDto>> strategyMap = new HashMap<>();
        fillStrategyMap(strategyMap, String.valueOf(exportData.size() + 1), 0);
        return strategyMap;
    }

    /**
     * 合并策略（可复用）
     *
     * @param strategyMap 保存合并列 或 合并行的容器
     * @param key         当前列 或 行
     * @param index       需要合并行 或 列
     */
    private static void fillStrategyMap(Map<String, List<RangeDto>> strategyMap, String key, int index) {
        // 初始化容器
        List<RangeDto> rangeDtoList = strategyMap.get(key) == null ? new ArrayList<RangeDto>() : strategyMap.get(key);
        boolean flag = false;
        for (RangeDto dto : rangeDtoList) {
            //分段list中是否有end索引是上一行索引的，如果有，则索引+1
            if (dto.getEnd() == index) {
                dto.setEnd(index + 1);
                flag = true;
            }
        }
        //如果没有，则新增分段
        if (!flag) {
            // 将当前行（列）向下合并一行，如果要合并多行（列），则index+n
            rangeDtoList.add(new RangeDto(index, index + 1));
        }
        strategyMap.put(key, rangeDtoList);
    }

}
