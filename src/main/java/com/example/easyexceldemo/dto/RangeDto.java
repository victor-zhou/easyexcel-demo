package com.example.easyexceldemo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RangeDto {
        int start;
        int end;
}
