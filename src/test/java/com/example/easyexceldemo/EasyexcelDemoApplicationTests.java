package com.example.easyexceldemo;

import com.example.easyexceldemo.dto.DataDmo;
import com.example.easyexceldemo.service.EasyExcelService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class EasyexcelDemoApplicationTests {

    @Autowired
    EasyExcelService easyExcelService;

    @Test
    void contextLoads() {
        List<DataDmo> exportList = new ArrayList<>();
        exportList.add(new DataDmo("A区","男性",1,1,1,1,1,1,1,1,8));
        exportList.add(new DataDmo("A区","女性",1,1,1,1,1,1,1,1,8));
        exportList.add(new DataDmo("B区","男性",1,1,1,1,1,1,1,1,8));
        exportList.add(new DataDmo("B区","女性",1,1,1,1,1,1,1,1,8));
        exportList.add(new DataDmo("C区","男性",1,1,1,1,1,1,1,1,8));
        exportList.add(new DataDmo("C区","女性",1,1,1,1,1,1,1,1,8));
        exportList.add(new DataDmo("D区","男性",1,1,1,1,1,1,1,1,8));
        exportList.add(new DataDmo("D区","女性",1,1,1,1,1,1,1,1,8));
        exportList.add(new DataDmo("E区","男性",1,1,1,1,1,1,1,1,8));
        exportList.add(new DataDmo("E区","女性",1,1,1,1,1,1,1,1,8));
        exportList.add(new DataDmo("F区","男性",1,1,1,1,1,1,1,1,8));
        exportList.add(new DataDmo("F区","女性",1,1,1,1,1,1,1,1,8));
        exportList.add(new DataDmo("G区","男性",1,1,1,1,1,1,1,1,8));
        exportList.add(new DataDmo("G区","女性",1,1,1,1,1,1,1,1,8));
        exportList.add(new DataDmo("合计","",14,14,14,14,14,14,14,14,112));
        String filepath = easyExcelService.exportData(exportList,"测试导出excel");
    }

}
